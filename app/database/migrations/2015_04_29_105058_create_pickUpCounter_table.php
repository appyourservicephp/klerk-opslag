<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePickUpCounterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pickUpCounter', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('trailer_id');
                        
                        $table->integer('trailer_pick_up');
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pickUpCounter');
	}

}
