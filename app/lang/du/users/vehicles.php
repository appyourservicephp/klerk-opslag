<?php

return array(
    'trailerType' => 'Type caravan',
    'locationOfStorage' => 'Opslag locatie',
    'license' => 'Vergunning',
    'deliveryDate' => 'Bezorg datum',
    'pickUpDate' => 'Ophaal datum',
    'serviceMoment' => 'Veiligheidscontrole/Onderhoudsbeurt',
    'message' => 'Wilt u de ophaaldatum wijzigen?'
);