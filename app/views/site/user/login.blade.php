@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
    <img src="{{asset('assets/img/logo.jpg')}}" width="350" style="margin-bottom: 20px" alt="logo"/>
    @if(Request::is('user/login/admin') )
        <h3>Admin login</h3>
        </div>
{{ Confide::makeLoginForm()->render() }}
  
    @else
	<h3>Login into your account</h3>
        </div>
        {{Form::open(array(
             'action' => 'UserController@postLoginEmail',
             'class' => 'form-signin'
                    ))}}
                  

<label class="sr-only " for="inputEmail">Email address</label>
<input id="inputEmail" class="form-control big" type="text" autofocus="" name='email' required="" placeholder="Email address">

<button class="btn btn-md btn-primary btn-block top" type="submit">Sign in</button>
{{ Form::close()}}
    @endif

@stop
