<?php

class Vehicle extends \Eloquent {

	// Add your validation rules here
	public static $rules = [               
            
	];

	// Don't forget to fill this array
	protected $fillable = ['trailerType', 'license', 
                                'locationStorage', 'deliveryDate', 
                                'serviceMoment', 'pickupDate'
            ];
        
        public function user()
        {
            return $this->belongsTo('User', 'id');
        }

}