$("document").ready(function () {
    // add company
    $("#trailerForm").on('keyup', '.search', function (e) {
        $(this).autocomplete({
            source: "/admin/users/create/autocomplete",
            minLength: 2,
            select: function (event, ui) {
                $(this).val(ui.item.value);
            }
        });
    });
    $('#submitForm').on('click', function () {
        $('.alert').empty();
        $.blockUI({css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }});

       
    })
    $("#trailerForm").on('submit', function (e) {

        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "/admin/users/store",
            dataType: 'json',
            data: $('#trailerForm').serialize(),
            error: function (data) {
                setTimeout($.unblockUI);
                if (data.status !== 200) {
                    if (data.responseJSON.errorsDate2) {
                        $('.ajax-errors').append('<p>The service moment can NOT be later then the pick-up date!</p>').show();
                    }
                    if (data.responseJSON.errorsDate) {
                        $('.ajax-errors').append('<p>Pick-up date can NOT be less then delivery date!</p>').show();
                    }
                    if (data.responseJSON.duplicate) {
                        $('.ajax-errors').append('<p>' + data.responseJSON.duplicate + '</p>').show();
                    }
                    $.each(data.responseJSON.errors, function (index, value) {
                        $('.ajax-errors').append('<li>' + value + '</li>').show();
                    });
                }


            },
            success: function (data) {
               

//                 $("#successMsg").html("<p>Vehicle(s) was successfully added!</p>").show();
                window.location.href = '/admin/users?success=1';
//                return true;  

            }
        }, "json");
    });







});